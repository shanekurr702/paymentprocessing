﻿using System;
using System.Globalization;

namespace PaymentProcessing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the array for processing.");
            double[] input;
            try
            {
                input = getInput();
                var finalBalance = calculateBalance(input);
                Console.WriteLine(generateFinalStatement(finalBalance));
            }
            catch
            {
                Console.WriteLine("Sorry, your input must be formatted as a numeric array.");
            }
        }

        /// <summary>
        /// Reads input from input stream and parses into an array of doubles for proper processing.
        /// </summary>
        /// <returns></returns>
        static Double[] getInput()
        {
            var input = Console.ReadLine();
            //clean input string and parse into array of strings
            input = input.TrimStart('[').TrimEnd(']');
            var stringArray = input.Split(',');
            // parse string input into numeric values
            var numArray = new Double[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
            {
                numArray[i] = Double.Parse(stringArray[i].Trim());
            }
            return numArray;
        }

        /// <summary>
        /// Calculates final balance of input array with first value being initial balance
        /// and each following balance being a charge.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>final balance</returns>
        static Double calculateBalance(Double[] input)
        {
            var runningBalance = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                runningBalance = runningBalance - input[i];
            }
            return Math.Round(runningBalance,2);
        }

        /// <summary>
        /// Generate final statement based on final balance
        /// </summary>
        /// <param name="balance"></param>
        /// <returns>final statement to be displayed to user.</returns>
        static string generateFinalStatement(Double balance)
        {
            if (balance < 0)
            {
                return ($"Change due {balance.ToString("C", CultureInfo.CurrentCulture).TrimStart('(').TrimEnd(')')}");
            }
            else if (Math.Round(balance,1) == 0)
            {
                return ("Balance paid in full");
            }
            else
            {
                return ($"Balance due {balance.ToString("C", CultureInfo.CurrentCulture)}");
            }
        }
    }
}
